/* SERVER */

#include "../src/RMPNode.h"
#pragma comment (lib, "../Release/RMP")
#pragma comment (lib, "CoreObject")
using namespace RMP;

class Server
{
	CoreObject		*_core;
	RMPNode			*_node;
	RMPSession		*_session;
	unsigned int	 _i;
	uint32_t		 _total_size;
public:
	Server(CoreObject *core) : _core(core)
	{
		_i = 0;
		_total_size = 0;
		fast_new1(RMPNode, _node, _core);
		_node->OnConnect.Attach<Server>(this, &Server::OnConnectCallback);
		_node->OnDisconnect.Attach<Server>(this, &Server::OnDisconnectCallback);
		_node->OnMessage.Attach<Server>(this, &Server::OnMessageCallback);
		_node->OnCongestionStart.Attach<Server>(this, &Server::OnCongestionStartCallback);
		_node->OnCongestionEnd.Attach<Server>(this, &Server::OnCongestionEndCallback);
	}
	~Server()
	{
		fast_delete(RMPNode, _node);
	}

	void Start(uint16_t port)
	{
		_node->Bind(port);
	}

	void OnConnectCallback(RMPSession *session)
	{
		_session = session;
		std::cout << "Connected    session: " << session->GetIP() << ":" << session->GetPort() << "." << std::endl;
	}

	void OnDisconnectCallback(RMPSession *session)
	{
		std::cout << "Disconnected session: " << session->GetIP() << ":" << session->GetPort() << "." << std::endl;
	}

	void OnMessageCallback(RMPSession *session, uint8_t *data, uint16_t &size)
	{
		_i++;
		_total_size += size;

		if(_i == 1000)
		{
			std::cout << "Done! Total size recceived = " << _total_size << "!" << std::endl;
		}
		
		//std::cout << _i << " : Received " << size << " byte(-s) from " <<
		//	session->GetIP() << ":" << session->GetPort() << std::endl;
	}

	void OnCongestionStartCallback(RMPSession *session)
	{
		std::cout << "CONGESTION STARTED IN SESSION: " << session->GetIP() << ":" << session->GetPort() << std::endl;
	}

	void OnCongestionEndCallback(RMPSession *session)
	{
		std::cout << "CONGESTION ENDED IN SESSION: " << session->GetIP() << ":" << session->GetPort() << std::endl;
	}
};

int main()
{
	CoreObject *_core = new CoreObject();
	
	Server *server = new Server(_core);
	server->Start(1357);

	std::cin.get();

	delete server;

	std::cin.get();

	delete _core;
	return 0;
}