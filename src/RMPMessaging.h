#ifndef RMPMESSAGING_H
#define RMPMESSAGING_H

#include "RMPConnection.h"

namespace RMP
{
	class RMPMessaging : public RMPConnection
	{
	public:
		RMPMessaging(CoreObject *);
		virtual ~RMPMessaging();
		
		void SystemSend(RMPSession *, RMPMessageType, uint8_t *, uint16_t);

	protected:
		Event5<RMPSession *, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &> OnOrderMessage;
		Event3<RMPSession *, uint32_t &, RMPMessage *> OnSystemSend;
		Event1<RMPSession *> OnDisconnectReq;
		Event2<RMPSession *, uint32_t &> OnDelivery;
		Event1<RMPSession *> OnQueueMessage;
		Event3<RMPSession *, uint8_t *, uint16_t &> OnSendMessage;
	private:
		void ParseMessage(RMPSession *, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &);
		void QueueMessage(RMPSession *);
	};
}

#endif //RMPMESSAGING_H