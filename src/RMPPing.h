#ifndef RMPPING_H
#define RMPPING_H

#include "RMPCongestion.h"

namespace RMP
{
	class RMPPing : public RMPCongestion
	{
	public:
		RMPPing(CoreObject *);
		virtual ~RMPPing();
	private:
		void Ping(RMPSession *);
	};
}

#endif //RMPPING_H