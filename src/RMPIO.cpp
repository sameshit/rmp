#include "RMPIO.h"
using namespace RMP;

RMPIO::RMPIO(CoreObject *core) : UdpSocket(core)
{
	OnRecvFrom.Attach<RMPIO>(this, &RMPIO::ReceivedFrom);
	fast_new1(RMPSessionManager, _rmp_session_manager, core);
}

RMPIO::~RMPIO()
{
	fast_delete(RMPSessionManager, _rmp_session_manager);
}

void RMPIO::ReceivedFrom(uint8_t *data, ssize_t &size, struct sockaddr *addr, socklen_t &len)
{
	uint32_t		msg_id;
	RMPMessageType	msg_type;
	uint16_t		msg_total_size;

	if(size < 5)
	{
		_LOG "Error: size < 5 = " << size EL;
		return;
	}
	
	msg_id			= Utils::GetBe32(data);
	msg_type		= (RMPMessageType)data[4];
	msg_total_size	= size - 5;

	OnSystemRecv(addr, msg_id, msg_type, &data[5], msg_total_size);
}