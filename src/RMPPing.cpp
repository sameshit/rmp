#include "RMPPing.h"
using namespace RMP;

RMPPing::RMPPing(CoreObject *core) : RMPCongestion(core)
{
	OnPingTask.Attach<RMPPing>(this, &RMPPing::Ping);
}

RMPPing::~RMPPing()
{
}

void RMPPing::Ping(RMPSession *session)
{
	uint8_t id[4] = { 0, 0, 0, 0 };
	RMPMessageType type = RMP_MT_PING;

	if(!SendTo(id, 4, (uint8_t *)&type, 1, 
		(struct sockaddr *)&session->_sock_addr, sizeof(struct sockaddr_in)))
	{
		LOG "Error: cannot send ping message!" EL;
	}
}