#include "RMPSpeed.h"
using namespace RMP;

RMPSpeed::RMPSpeed(CoreObject *core) : RMPGlew(core)
{
	//OnSystemSend.Attach<RMPSpeed>(this, &RMPSpeed::SystemSendSpeed);
	OnSendMessage.Attach<RMPSpeed>(this, &RMPSpeed::SendSpeed);
	OnRecv.Attach<RMPSpeed>(this, &RMPSpeed::SystemRecvSpeed);
	OnMessage.Attach<RMPSpeed>(this, &RMPSpeed::RecvSpeed);

	fast_new2(Timer, _timer_speed, core, RMP_SPEED_TIMEOUT);
	_timer_speed->OnTimer.Attach<RMPSpeed>(this, &RMPSpeed::SpeedTimeout);
}

RMPSpeed::~RMPSpeed()
{
	fast_delete(Timer, _timer_speed);
}

void RMPSpeed::SpeedTimeout()
{
	std::set<RMPSession *> sessions;
	std::set<RMPSession *>::iterator it;

	_rmp_session_manager->GetAllSessions(&sessions);

	for(it = sessions.begin(); it != sessions.end(); ++it)
	{
		(*it)->_speed_cur_download		= (*it)->_speed_download;
		(*it)->_speed_cur_upload		= (*it)->_speed_upload;
		(*it)->_speed_cur_sys_download	= (*it)->_speed_sys_download;
		(*it)->_speed_cur_sys_upload	= (*it)->_speed_sys_upload;

		(*it)->_speed_download			= 0;
		(*it)->_speed_upload			= 0;
		(*it)->_speed_sys_download		= 0;
		(*it)->_speed_sys_upload		= 0;
	}
}

void RMPSpeed::SystemSendSpeed(RMPSession *session, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &size)
{
	session->_speed_sys_upload		+= size;
}

void RMPSpeed::SendSpeed(RMPSession *session, uint8_t *, uint16_t &size)
{
	session->_speed_upload			+= size;
}

void RMPSpeed::SystemRecvSpeed(RMPSession *session, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &size)
{
	session->_speed_sys_download	+= size;
}

void RMPSpeed::RecvSpeed(RMPSession *session, uint8_t *, uint16_t &size)
{
	session->_speed_download		+= size;
}
