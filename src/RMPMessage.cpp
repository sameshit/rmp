#include "RMPMessage.h"
using namespace RMP;

RMPMessage::RMPMessage(uint32_t id, RMPMessageType type, uint8_t *data, ssize_t size)
{
	this->_rmp_msg_id		= id;
	this->_rmp_msg_type		= type;
	this->_rmp_msg_size		= size;

	this->_rmp_msg_data = new uint8_t [size];
	memcpy(this->_rmp_msg_data, data, size);
}

RMPMessage::RMPMessage(const RMPMessage &rmpMessage)
{
	this->_rmp_msg_id			= rmpMessage._rmp_msg_id;
	this->_rmp_msg_type			= rmpMessage._rmp_msg_type;
	this->_rmp_msg_size			= rmpMessage._rmp_msg_size;
	this->_rmp_msg_send_time	= rmpMessage._rmp_msg_send_time;

	this->_rmp_msg_data = new uint8_t [rmpMessage._rmp_msg_size];
	memcpy(this->_rmp_msg_data, rmpMessage._rmp_msg_data, rmpMessage._rmp_msg_size);
}

RMPMessage::~RMPMessage(void)
{
	delete [] this->_rmp_msg_data;
}
