#ifndef RMPRTT_H
#define RMPRTT_H

#include "RMPDelivery.h"

namespace RMP
{
	#define RMP_RTT_AVG_COEF	.8
	#define RMP_RTT_AVG_COUNT	20

	class RMPRtt : public RMPDelivery
	{
	private:
		std::vector<double> _coefs;
	public:
		RMPRtt(CoreObject *);
		virtual ~RMPRtt();
	private:
		void PushRtt(RMPSession *session, uint32_t &id);
	};
}

#endif //RMPRTT_H