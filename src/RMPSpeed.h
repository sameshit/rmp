#ifndef RMPSPEED_H
#define RMPSPEED_H

#include "RMPGlew.h"

namespace RMP
{
	#define RMP_SPEED_TIMEOUT 1000

	class RMPSpeed : public RMPGlew
	{
		Timer *_timer_speed;
	public:
		RMPSpeed(CoreObject *core);
		virtual ~RMPSpeed();
	private:
		void SpeedTimeout();

		void SystemSendSpeed(RMPSession*, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &);
		void SendSpeed(RMPSession *, uint8_t *, uint16_t &);
		void SystemRecvSpeed(RMPSession *, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &);
		void RecvSpeed(RMPSession *, uint8_t *, uint16_t &);
	};
}

#endif //RMPSPEED_H