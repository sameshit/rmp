#include "RMPGlew.h"
using namespace RMP;

RMPGlew::RMPGlew(CoreObject *core) : RMPOrder(core)
{
	_glew_state = RMP_GS_IDLE;

	OnOrderedMessage.Attach<RMPGlew>(this, &RMPGlew::ParseOrderedMessage);
}

RMPGlew::~RMPGlew()
{
	
}

void RMPGlew::ParseOrderedMessage(RMPSession *session, RMPMessageType &type, uint8_t *data, uint16_t &size)
{
	switch(type)
	{
	case RMP_MT_MSG:
		{
			if(session->_glew_state == RMP_GS_IDLE)
				OnMessage(session, data, size);
			else
			{
				LOG "Error: glew state = " << session->_glew_state << " in session "
					<< session->GetIP() << ":" << session->GetPort() << " and received MSG!" EL;
			}
			break;
		}
	case RMP_MT_MSG_BEGIN:
		{
			if(session->_glew_state == RMP_GS_IDLE)
				BeginMessage(session, data, size);
			else
			{
				LOG "Error: glew state = " << session->_glew_state << " in session "
					<< session->GetIP() << ":" << session->GetPort() << " and received MSG_BEGIN!" EL;
			}
			break;
		}
	case RMP_MT_MSG_PART:
		{
			if(session->_glew_state == RMP_GS_PART)
				AppendMessage(session, data, size);
			else
			{
				LOG "Error: glew state = " << session->_glew_state << " in session "
					<< session->GetIP() << ":" << session->GetPort() << " and received MSG_PART!" EL;
			}
			break;
		}
	case RMP_MT_MSG_END:
		{
			if(session->_glew_state == RMP_GS_PART)
				EndMessage(session, data, size);
			else
			{
				LOG "Error: glew state = " << session->_glew_state << " in session "
					<< session->GetIP() << ":" << session->GetPort() << " and received MSG_END!" EL;
			}
			break;
		}
	default:
		LOG "Error: invalid msg type in parse ordered message = " << type EL;
		break;
	}
}

void RMPGlew::BeginMessage(RMPSession *session, uint8_t *data, uint16_t &size)
{
	uint32_t total_size = Utils::GetBe16(data);

	session->_glew_state = RMP_GS_PART;
	session->_glew_buffer_size = total_size;
	session->_glew_buffer = fast_alloc(uint8_t, total_size);
	memcpy(session->_glew_buffer, &data[2], size - 2);
	session->_glew_buffer_seek = size - 2;
}

void RMPGlew::AppendMessage(RMPSession *session, uint8_t *data, uint16_t &size)
{
	memcpy(&session->_glew_buffer[session->_glew_buffer_seek], data, size);
	session->_glew_buffer_seek += size;
}

void RMPGlew::EndMessage(RMPSession *session, uint8_t *data, uint16_t &size)
{
	memcpy(&session->_glew_buffer[session->_glew_buffer_seek], data, size);
	session->_glew_buffer_seek += size;

	if(session->_glew_buffer_seek != session->_glew_buffer_size)
	{
		LOG "Error: message end, but seek = " << session->_glew_buffer_seek
			<< " and size = " << session->_glew_buffer_size
			<< " in session " << session->GetIP() << ":" << session->GetPort() EL;
	}
	
	OnMessage(session, session->_glew_buffer, session->_glew_buffer_size);

	fast_free(session->_glew_buffer);
	session->_glew_buffer = 0;
	session->_glew_buffer_seek = 0;
	session->_glew_buffer_size = 0;
	session->_glew_state = RMP_GS_IDLE;
}

bool RMPGlew::Send(RMPSession *session, uint8_t *data, uint16_t &size)
{
	if(session->_congested)
		return false;

	if(size > RMP_MSG_LEN)
	{
		uint32_t seek = 0;

		while(seek != size)
		{
			if(seek == 0)
			{
				uint8_t info [RMP_MSG_LEN];
				Utils::PutBe16(info, size);

				memcpy(&info[2], data, RMP_MSG_LEN - 2);
				
				SystemSend(session, RMP_MT_MSG_BEGIN, info, RMP_MSG_LEN);

				_glew_state = RMP_GS_PART;
				seek += RMP_MSG_LEN - 2;
			}
			else
			{
				if(size - seek >= RMP_MSG_LEN)
				{
					SystemSend(session, RMP_MT_MSG_PART, &data[seek], RMP_MSG_LEN);

					seek += RMP_MSG_LEN;
				}
				else
				{
					SystemSend(session, RMP_MT_MSG_END, &data[seek], size - seek);

					seek += size - seek;
					_glew_state = RMP_GS_IDLE;
				}
			}
		}
	}
	else
	{
		if(_glew_state == RMP_GS_IDLE)
			SystemSend(session, RMP_MT_MSG, data, size);
		else
		{
			LOG "Error: state = " << _glew_state << ", trying to send invalid msg type!" EL;
			return false;
		}
	}

	return true;
}