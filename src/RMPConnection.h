#ifndef RMPCONNECTION_H
#define RMPCONNECTION_H

#include "RMPIO.h"

namespace RMP
{
	#define RMP_CONNECTION_TIMEOUT 2000

	class RMPConnection : public RMPIO
	{
	private:
		TimeTask					*_task_disconnect;
		std::queue<RMPSession *>	 _sessions;
	public:
		RMPConnection(CoreObject *);
		virtual ~RMPConnection();

		RMPSession *Connect(struct sockaddr *);
		RMPSession *Connect(const char *, uint16_t);
		void Disconnect(RMPSession *);
		
		Event1<RMPSession *> OnConnect;
		Event1<RMPSession *> OnDisconnect;

	protected:
		Event5<RMPSession *, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &> OnRecv;
		Event1<RMPSession *> OnPingTask;
		Event1<RMPSession *> OnDeliveryTask;
	private:
		void SystemRecv(struct sockaddr *, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &);
		void DisconnectSession(RMPSession *);
		void PingSession(RMPSession *);
		void DeliverySession(RMPSession *);

		void TaskDisconnect();
	};
}

#endif //RMPCONNECTION_H