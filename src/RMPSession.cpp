#include "RMPSession.h"
using namespace RMP;

RMPSession::RMPSession(struct sockaddr *addr, CoreObject *core)
{
	this->_core = core;
	_scheduler = _core->GetScheduler();

	this->_last_recv_id = 1;
	this->_last_sent_id = 1;

	memcpy(&this->_sock_addr, addr, sizeof(struct sockaddr_in));

	_last_rtt	= 0;
	_avg_rtt	= 0;

	_speed_download				= 0;
	_speed_upload				= 0;
	_speed_sys_download			= 0;
	_speed_sys_upload			= 0;
	_speed_cur_download			= 0;
	_speed_cur_upload			= 0;
	_speed_cur_sys_download		= 0;
	_speed_cur_sys_upload		= 0;

	_glew_state			= RMP_GS_IDLE;
	_glew_buffer		= 0;
	_glew_buffer_seek	= 0;
	_glew_buffer_size	= 0;

	_send_queue_size			= 20480;
	_send_queue_speed			= 1024000;
	_send_queue_free_space		= 4096;//65535;

	fast_new(Mutex, _mutex_send);

	_congested	= false;

	fast_new2(TimeTask, _task_connection, _core, RMP_RTT_START_VAL);
	_task_connection->OnTimeTask.Attach<RMPSession>(this, &RMPSession::TaskConnection);
	_scheduler->Add(_task_connection);

	fast_new2(TimeTask, _task_delivery, _core, RMP_RTT_START_VAL / 2);
	_task_delivery->OnTimeTask.Attach<RMPSession>(this, &RMPSession::TaskDelivery);
	_scheduler->Add(_task_delivery);

	fast_new2(TimeTask, _task_ping, _core, RMP_RTT_START_VAL / 2);
	_task_ping->OnTimeTask.Attach<RMPSession>(this, &RMPSession::TaskPing);
	_scheduler->Add(_task_ping);
}

RMPSession::~RMPSession()
{
	std::map<uint32_t, RMPMessage *, OrderIdLess>::iterator it_received;
	std::map<uint32_t, RMPMessage *, OrderIdLess>::iterator it_delivery;

	if(!_recv_messages.empty())
	{
		for(it_received = _recv_messages.begin(); it_received != _recv_messages.end(); ++it_received)
		{
			fast_delete(RMPMessage, (*it_received).second);
		}
		_recv_messages.clear();
	}

	if(!_delivery_messages.empty())
	{
		for(it_delivery = _delivery_messages.begin(); it_delivery != _delivery_messages.end(); ++it_delivery)
		{
			fast_delete(RMPMessage, (*it_delivery).second);
		}
		_delivery_messages.clear();
	}

	_mutex_send->Lock();
	while(!_send_queue.empty())
	{
		fast_delete(RMPMessage, _send_queue.front());
		_send_queue.pop();
	}
	_mutex_send->UnLock();

	_rtts.clear();

	_scheduler->Delete(_task_ping);
	fast_delete(TimeTask, _task_ping);

	_scheduler->Delete(_task_delivery);
	fast_delete(TimeTask, _task_delivery);

	_scheduler->Delete(_task_connection);
	fast_delete(TimeTask, _task_connection);

	fast_delete(Mutex, _mutex_send);

	if(_glew_buffer != 0)
		fast_free(_glew_buffer);
}

void RMPSession::GetAllRecvMessages(std::map<uint32_t, RMPMessage *, OrderIdLess> *messages)
{
	std::map<uint32_t, RMPMessage *, OrderIdLess>::iterator it;

	for(it = _recv_messages.begin(); it != _recv_messages.end(); ++it)
	{
		messages->insert(std::make_pair((*it).first, (*it).second));
	}
}

void RMPSession::GetAllDeliveryMessages(std::map<uint32_t, RMPMessage *, OrderIdLess> *messages)
{
	std::map<uint32_t, RMPMessage *, OrderIdLess>::iterator it;

	for(it = _delivery_messages.begin(); it != _delivery_messages.end(); ++it)
	{
		messages->insert(std::make_pair((*it).first, (*it).second));
	}
}

void RMPSession::TaskConnection()
{
	Time now;

	if(_avg_rtt < RMP_RTT_MIN_VALUE)
	{
		if(now - _last_recv_time > RMP_RTT_MIN_VALUE * 4)
		{
			if(!_congested)
				OnDisconnectTask(this);
			return;
		}
		else
			_task_connection->SetTimeout(RMP_RTT_MIN_VALUE * 4);
	}
	else
	{
		if(now - _last_recv_time > _avg_rtt * 4)
		{
			if(!_congested)
				OnDisconnectTask(this);
			return;
		}
		else
			_task_connection->SetTimeout(_avg_rtt * 4);
	}

	_scheduler->Add(_task_connection);
}

void RMPSession::TaskPing()
{
	if(_avg_rtt < RMP_RTT_MIN_VALUE)
		_task_ping->SetTimeout(RMP_RTT_MIN_VALUE);
	else
		_task_ping->SetTimeout(_avg_rtt);

	OnPingTask(this);
	_scheduler->Add(_task_ping);
}

void RMPSession::TaskDelivery()
{
	if(_avg_rtt < RMP_RTT_MIN_VALUE)
		_task_delivery->SetTimeout(RMP_RTT_MIN_VALUE);
	else
		_task_delivery->SetTimeout(_avg_rtt);

	OnDeliveryTask(this);
	_scheduler->Add(_task_delivery);
}