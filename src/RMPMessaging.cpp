#include "RMPMessaging.h"
using namespace RMP;

RMPMessaging::RMPMessaging(CoreObject *core) : RMPConnection(core)
{
	OnRecv.Attach<RMPMessaging>(this, &RMPMessaging::ParseMessage);
	OnQueueMessage.Attach<RMPMessaging>(this, &RMPMessaging::QueueMessage);
}

RMPMessaging::~RMPMessaging()
{

}

void RMPMessaging::SystemSend(RMPSession *session, RMPMessageType type, uint8_t *data, uint16_t size)
{
	RMPMessage *msg;
	bool queue_empty;

	if(type == RMP_MT_PING || type == RMP_MT_DELIVERY)
	{
		if(!SendTo(data, size, (uint8_t *)&type, 1, (struct sockaddr *)&session->_sock_addr, sizeof(struct sockaddr_in)))
		{
			LOG "Error: cannot send ping or delivery message!" EL;
		}
	}
	else
	{
		fast_new4(RMPMessage, msg, session->_last_sent_id, type, data, size);
		session->_last_sent_id++;

		session->_mutex_send->Lock();
		queue_empty = session->_send_queue.empty();
		session->_send_queue.push(msg);	
		session->_mutex_send->UnLock();

		if(queue_empty)
			OnQueueMessage(session);
	}
}

void RMPMessaging::ParseMessage(RMPSession *session, uint32_t &id, RMPMessageType &type, uint8_t *data, uint16_t &size)
{
	switch(type)
	{
	case RMP_MT_PING:
		break;
	case RMP_MT_DELIVERY:
		OnDelivery(session, id);
		break;
	case RMP_MT_MSG:
	case RMP_MT_MSG_BEGIN:
	case RMP_MT_MSG_PART:
	case RMP_MT_MSG_END:
		OnOrderMessage(session, id, type, data, size);
		break;
	case RMP_MT_DISCONNECT:
		OnDisconnectReq(session);
		Disconnect(session);
		break;
	default:
		_LOG "Error: invalid type = " << type EL;
		break;
	}
}

void RMPMessaging::QueueMessage(RMPSession *session)
{
	RMPMessage		*msg;
	uint8_t			 id[4];

	session->_mutex_send->Lock();
	while(!session->_send_queue.empty() 
		&& (msg = session->_send_queue.front())->_rmp_msg_size < session->_send_queue_free_space)
	{
		session->_send_queue_free_space -= msg->_rmp_msg_size;

		Utils::PutBe32(id, msg->_rmp_msg_id);
		if(!SendTo(id, 4, (uint8_t *)&msg->_rmp_msg_type, 1, 
			msg->_rmp_msg_data, msg->_rmp_msg_size, 
			(struct sockaddr *)&session->_sock_addr, sizeof(struct sockaddr_in)))
		{
			LOG "Error: cannot send message with id " << msg->_rmp_msg_id EL;
		}

		OnSystemSend(session, msg->_rmp_msg_id, msg);
		OnSendMessage(session, msg->_rmp_msg_data, msg->_rmp_msg_size);

		session->_send_queue.pop();
	}
	session->_mutex_send->UnLock();
}