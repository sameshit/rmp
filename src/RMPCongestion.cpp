#include "RMPCongestion.h"
using namespace RMP;

RMPCongestion::RMPCongestion(CoreObject *core) : RMPRtt(core)
{
	OnDeliveryResend.Attach<RMPCongestion>(this, &RMPCongestion::StartControl);
	OnDelivery.Attach<RMPCongestion>(this, &RMPCongestion::EndControl);
}

RMPCongestion::~RMPCongestion()
{

}

void RMPCongestion::StartControl(RMPSession *session, uint32_t &id)
{
	std::map<uint32_t, uint32_t>::iterator it;

	it = session->_congested_ids.find(id);
	
	if(it == session->_congested_ids.end())
		session->_congested_ids.insert(std::make_pair(id, 1));
	else
	{
		(*it).second++;

		if((*it).second == RMP_CONGESTION_START_VALUE && !session->_congested)
		{
			session->_congested = true;
			OnCongestionStart(session);
		}
	}
}

void RMPCongestion::EndControl(RMPSession *session, uint32_t &id)
{
	std::map<uint32_t, uint32_t>::iterator it;

	it = session->_congested_ids.find(id);
	if(it == session->_congested_ids.end())
		return;

	session->_congested_ids.erase(it);

	if(session->_congested && session->_congested_ids.size() == 0)
	{
		session->_congested = false;
		OnCongestionEnd(session);
	}
}