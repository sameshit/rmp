#include "RMPSessionManager.h"
using namespace RMP;

RMPSessionManager::RMPSessionManager(CoreObject *core)
{
	this->_core = core;
}

RMPSessionManager::~RMPSessionManager()
{
	std::set<RMPSession *>::iterator it;

	for(it = _rmp_sessions.begin(); it != _rmp_sessions.end(); ++it)
	{
		fast_delete(RMPSession, (*it));
	}

	_rmp_sessions.clear();
}

RMPSession *RMPSessionManager::Insert(struct sockaddr *addr)
{
	RMPSession *session = 0;
	
	fast_new2(RMPSession, session, addr, _core);
	_rmp_sessions.insert(session);
	
	return session;
}

void RMPSessionManager::Delete(RMPSession *session)
{
	std::set<RMPSession *>::iterator it;

	it = _rmp_sessions.find(session);
	if(it == _rmp_sessions.end())
	{
		LOG "Error: cannot find session to delete it!" EL;
		return;
	}

	fast_delete(RMPSession, (*it));
	_rmp_sessions.erase(it);
}

RMPSession *RMPSessionManager::Find(struct sockaddr *addr)
{
	std::set<RMPSession *>::iterator it;
	struct sockaddr_in addr_find;

	memcpy(&addr_find, addr, sizeof(struct sockaddr_in));
	for(it = _rmp_sessions.begin(); it != _rmp_sessions.end(); ++it)
	{
		if(addr_find.sin_addr.s_addr == (*it)->_sock_addr.sin_addr.s_addr
			&& addr_find.sin_port == (*it)->_sock_addr.sin_port
			&& addr_find.sin_family == (*it)->_sock_addr.sin_family)
		{
			return (*it);
		}
	}

	return 0;
}

void RMPSessionManager::GetAllSessions(std::set<RMPSession *> *sessions)
{
	std::set<RMPSession *>::iterator it;

	for(it = _rmp_sessions.begin(); it != _rmp_sessions.end(); ++it)
	{
		sessions->insert((*it));
	}
}