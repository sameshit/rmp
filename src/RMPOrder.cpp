#include "RMPOrder.h"
using namespace RMP;

RMPOrder::RMPOrder(CoreObject *core) : RMPPing(core)
{
	OnOrderMessage.Attach<RMPOrder>(this, &RMPOrder::OrderMessage);
}

RMPOrder::~RMPOrder()
{
}

void RMPOrder::OrderMessage(RMPSession *session, uint32_t &id, RMPMessageType &type, uint8_t *data, uint16_t &size)
{
	std::map<uint32_t, RMPMessage *, OrderIdLess>::iterator it;
	
	std::set<uint32_t> delete_ids;
	std::set<uint32_t>::iterator delete_it;
	
	uint32_t			 msg_id;
	RMPMessageType		 msg_type;
	uint16_t			 msg_size;
	uint8_t				*msg_data;

	if(session->_last_recv_id == id)
	{
		OnOrderedMessage(session, type, data, size);
		session->_last_recv_id++;

		for(it = session->_recv_messages.begin(); it != session->_recv_messages.end(); ++it)
		{
			while((*it).first < session->_last_recv_id)
			{
				delete_ids.insert((*it).first);
				it = session->_recv_messages.begin();

				if(it == session->_recv_messages.end())
					break;
			}

			msg_id = (*it).first;

			if(msg_id == session->_last_recv_id)
			{
				msg_type = (*it).second->GetType();
				msg_size = (*it).second->GetSize();
				msg_data = (*it).second->GetData();

				OnOrderedMessage(session, msg_type, msg_data, msg_size);
				session->_last_recv_id++;

				delete_ids.insert(msg_id);
			}
			else
				break;
		}

		for(delete_it = delete_ids.begin(); delete_it != delete_ids.end(); ++delete_it)
			session->_recv_messages.erase((*delete_it));
		delete_ids.clear();
	}
	else
	{
		RMPMessage *msg;

		fast_new4(RMPMessage, msg, id, type, data, size);
		session->_recv_messages.insert(std::make_pair(id, msg));
	}
}