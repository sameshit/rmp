#ifndef RMPMESSAGE_H
#define RMPMESSAGE_H

#include <CoreObject/CoreObject.h>
#include <CoreObject/core/time/TimeTask.h>
using namespace CoreObjectLib;

namespace RMP
{
	enum RMPMessageType
	{
		RMP_MT_PING			= 1,
		RMP_MT_DELIVERY		= 2,
		RMP_MT_MSG			= 3,
		RMP_MT_DISCONNECT	= 4,
		RMP_MT_MSG_BEGIN	= 5,
		RMP_MT_MSG_PART		= 6,
		RMP_MT_MSG_END		= 7,
	};

	class RMPMessage
	{
	private:
		uint32_t		 _rmp_msg_id;
		RMPMessageType	 _rmp_msg_type;
		uint8_t			*_rmp_msg_data;
		uint16_t		 _rmp_msg_size;
		Time			 _rmp_msg_send_time;
	public:
		RMPMessage(uint32_t, RMPMessageType, uint8_t *, ssize_t);
		RMPMessage(const RMPMessage &);
		~RMPMessage();

		uint32_t		 GetID()		{ return this->_rmp_msg_id;			}
		RMPMessageType	 GetType()		{ return this->_rmp_msg_type;		}
		uint8_t			*GetData()		{ return this->_rmp_msg_data;		}
		uint16_t		 GetSize()		{ return this->_rmp_msg_size;		}
		Time			 GetSendTime()	{ return this->_rmp_msg_send_time;	}

		void			 UpdateSendTime() { this->_rmp_msg_send_time.Update(); }

		friend class RMPMessaging;
	};
}

#endif //RMPMESSAGE_H