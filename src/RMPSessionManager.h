#ifndef RMPSESSIONMANAGER_H
#define RMPSESSIONMANAGER_H

#include "RMPSession.h"

namespace RMP
{
	class RMPSessionManager
	{
	private:
		CoreObject				*_core;
		std::set<RMPSession *>	 _rmp_sessions;
	public:
		RMPSessionManager(CoreObject *);
		~RMPSessionManager();

		RMPSession *Insert(struct sockaddr *);
		RMPSession *Find(struct sockaddr *);

		void Delete(RMPSession *);
		
		void GetAllSessions(std::set<RMPSession *> *);
	};
}

#endif //RMPSESSIONMANAGER_H