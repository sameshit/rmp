#ifndef RMPORDER_H
#define RMPORDER_H

#include "RMPPing.h"

namespace RMP
{
	class RMPOrder : public RMPPing
	{
	public:
		RMPOrder(CoreObject *);
		virtual ~RMPOrder();
	protected:
		Event4<RMPSession *, RMPMessageType &, uint8_t *, uint16_t &> OnOrderedMessage;
	private:
		void OrderMessage(RMPSession *, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &);
	};
}

#endif //RMPORDER_H