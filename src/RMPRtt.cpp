#include "RMPRtt.h"
using namespace RMP;

RMPRtt::RMPRtt(CoreObject *core) : RMPDelivery(core)
{
	OnRtt.Attach<RMPRtt>(this, &RMPRtt::PushRtt);

	_coefs.push_back(1);
	for(int i = 1; i < RMP_RTT_AVG_COUNT; i++)
		_coefs.push_back(RMP_RTT_AVG_COEF * _coefs.back());
}

RMPRtt::~RMPRtt()
{
	_coefs.clear();
}

void RMPRtt::PushRtt(RMPSession *session, uint32_t &id)
{
	std::map<uint32_t, RMPMessage *, OrderIdLess>::iterator it_msg;
	std::list<uint64_t>::iterator it_rtts;
	
	uint32_t	index	= 0;
	double		top_sum = 0;
	double		bot_sum = 0;
	
	Time time;

	if(session->_rtts.size() >= RMP_RTT_AVG_COUNT)
		session->_rtts.pop_back();

	it_msg = session->_delivery_messages.find(id);

	if(it_msg == session->_delivery_messages.end())
	{
		_LOG "RTT Error: cannot find message with id " << id EL;
		return;
	}

	session->_last_rtt = time - (*it_msg).second->GetSendTime();
	session->_rtts.push_front(session->_last_rtt);

	for(it_rtts = session->_rtts.begin(); it_rtts != session->_rtts.end(); it_rtts++, index++)
	{
		top_sum += (double)((*it_rtts) * _coefs[index]);
		bot_sum += _coefs[index];
	}

	session->_avg_rtt = (uint64_t)(top_sum / bot_sum);
}