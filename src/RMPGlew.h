#ifndef RMPGLEW_H
#define RMPGLEW_H

#include "RMPOrder.h"

namespace RMP
{
	#define RMP_MSG_LEN 1000

	class RMPGlew : public RMPOrder
	{
	private:
		RMPGlewState	_glew_state;
	public:
		RMPGlew(CoreObject *);
		virtual ~RMPGlew();

		bool Send(RMPSession *, uint8_t *, uint16_t &);

		Event3<RMPSession *, uint8_t *, uint16_t &> OnMessage;
	private:
		void ParseOrderedMessage(RMPSession *, RMPMessageType &, uint8_t *, uint16_t &);

		void BeginMessage(RMPSession *, uint8_t *, uint16_t &);
		void AppendMessage(RMPSession *, uint8_t *, uint16_t &);
		void EndMessage(RMPSession *, uint8_t *, uint16_t &);
	};
}

#endif //RMPGLEW_H