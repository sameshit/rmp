#include "RMPDelivery.h"
using namespace RMP;

RMPDelivery::RMPDelivery(CoreObject *core) : RMPMessaging(core)
{
	OnSystemSend.Attach<RMPDelivery>(this, &RMPDelivery::DeliveryPush);
	OnDelivery.Attach<RMPDelivery>(this, &RMPDelivery::DeliveryPop);
	OnRecv.Attach<RMPDelivery>(this, &RMPDelivery::DeliverySend);

	OnDeliveryTask.Attach<RMPDelivery>(this, &RMPDelivery::DeliveryTask);
}

RMPDelivery::~RMPDelivery()
{

}

void RMPDelivery::DeliveryPush(RMPSession *session, uint32_t &id, RMPMessage *message)
{
	session->_delivery_messages.insert(std::make_pair(id, message));
}

void RMPDelivery::DeliveryPop(RMPSession *session, uint32_t &id)
{
	RMPMessage *msg;
	std::map<uint32_t, RMPMessage *, OrderIdLess>::iterator it;
	
	it = session->_delivery_messages.find(id);

	if(it == session->_delivery_messages.end())
	{
		LOG "Error: unknown delivery message with id " << id EL;
		return;
	}

	msg = (*it).second;

	session->_send_queue_free_space += msg->GetSize();
	OnQueueMessage(session);

	OnRtt(session, id);
	
	fast_delete(RMPMessage, msg);
	session->_delivery_messages.erase(it);
}

void RMPDelivery::DeliverySend(RMPSession *session, uint32_t &id, RMPMessageType &type, uint8_t *, uint16_t &)
{
	if(type == RMP_MT_PING || type == RMP_MT_DELIVERY)
		return;

	uint8_t m_id [4];
	Utils::PutBe32(m_id, id);
	RMPMessageType m_type = RMP_MT_DELIVERY;

	if(!SendTo(m_id, 4, (uint8_t *)&m_type, 1, 
		(struct sockaddr *)&session->_sock_addr, sizeof(struct sockaddr_in)))
	{
		LOG "Error: cannot send delivery message with id " << id EL;
	}
}

void RMPDelivery::DeliveryTask(RMPSession *session)
{
	uint64_t		 timeout;

	uint32_t		 msg_id;
	RMPMessageType	 msg_type;
	uint8_t			*msg_data;
	ssize_t			 msg_size;

	std::map<uint32_t, RMPMessage *, OrderIdLess> messages;
	std::map<uint32_t, RMPMessage *, OrderIdLess>::iterator it;

	session->GetAllDeliveryMessages(&messages);
	timeout = ((session->_avg_rtt < RMP_RTT_MIN_VALUE) ? RMP_RTT_MIN_VALUE : session->_avg_rtt);
	Time now;

	for(it = messages.begin(); it != messages.end(); ++it)
	{
		if(now - (*it).second->GetSendTime() > timeout)
		{
			msg_id	 = (*it).first;
			msg_type = (*it).second->GetType();
			msg_data = (*it).second->GetData();
			msg_size = (*it).second->GetSize();

			uint8_t id [4];
			Utils::PutBe32(id, msg_id);

			if(!SendTo(id, 4, (uint8_t *)&msg_type, 1, msg_data, msg_size, 
				(struct sockaddr *)&session->_sock_addr, sizeof(struct sockaddr_in)))
			{
				LOG "Error: cannot resend msg with id " << msg_id EL;
			}

			OnDeliveryResend(session, msg_id);
		}
	}

	messages.clear();
}