#ifndef RMPCONGESTION_H
#define RMPCONGESTION_H

#include "RMPRtt.h"

namespace RMP
{
	#define RMP_CONGESTION_START_VALUE 3

	class RMPCongestion : public RMPRtt
	{
	public:
		RMPCongestion(CoreObject *);
		virtual ~RMPCongestion();

		Event1<RMPSession *> OnCongestionStart;
		Event1<RMPSession *> OnCongestionEnd;
	private:
		void StartControl(RMPSession *, uint32_t &);
		void EndControl(RMPSession *, uint32_t &);
	};
}

#endif //RMPCONGESTION_H