#ifndef RMPNODE_H
#define RMPNODE_H

#include "RMPSpeed.h"

namespace RMP
{
	class RMPNode : public RMPSpeed
	{
	public:
		RMPNode(CoreObject *);
		virtual ~RMPNode();

		void GetAllSessions(std::set<RMPSession *> *sessions)
		{
			_rmp_session_manager->GetAllSessions(sessions);
		}
	};
}

#endif //RMPNODE_H