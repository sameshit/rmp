#ifndef RMPIO_H
#define RMPIO_H

#pragma comment(lib, "WS2_32")
#pragma comment(lib, "CoreObject")
#include <CoreObject/CoreObject.h>
#include <CoreObject/core/protocols/UDP/UdpSocket.h>
#include <CoreObject/core/time/Timer.h>
#include "RMPSessionManager.h"
using namespace CoreObjectLib;

namespace RMP
{
	class RMPIO : public UdpSocket
	{
	public:
		RMPIO(CoreObject *);
		virtual ~RMPIO();
	protected:
		RMPSessionManager *_rmp_session_manager;
		Event5<struct sockaddr *, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &> OnSystemRecv;
	private:
		void ReceivedFrom(uint8_t *, ssize_t &, struct sockaddr *, socklen_t &);
	};
}

#endif //RMPIO_H