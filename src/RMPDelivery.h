#ifndef RMPDELIVERY_H
#define RMPDELIVERY_H

#include "RMPMessaging.h"

namespace RMP
{
	class RMPDelivery : public RMPMessaging
	{
	public:
		RMPDelivery(CoreObject *);
		virtual ~RMPDelivery();

	protected:
		Event2<RMPSession *, uint32_t &> OnRtt;
		Event2<RMPSession *, uint32_t &> OnDeliveryResend;
	private:
		void DeliveryPush(RMPSession *, uint32_t &, RMPMessage *);
		void DeliveryPop(RMPSession *, uint32_t &);
		void DeliveryTask(RMPSession *);
		void DeliverySend(RMPSession *, uint32_t &, RMPMessageType &, uint8_t *, uint16_t &);
	};
}

#endif //RMPDELIVERY_H