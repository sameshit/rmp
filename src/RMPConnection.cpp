#include "RMPConnection.h"
using namespace RMP;

RMPConnection::RMPConnection(CoreObject *core) : RMPIO(core)
{
	OnSystemRecv.Attach<RMPConnection>(this, &RMPConnection::SystemRecv);

	fast_new2(TimeTask, _task_disconnect, core, 0);
	_task_disconnect->OnTimeTask.Attach<RMPConnection>(this, &RMPConnection::TaskDisconnect);
}

RMPConnection::~RMPConnection()
{
	fast_delete(TimeTask, _task_disconnect);
}

RMPSession *RMPConnection::Connect(struct sockaddr *addr)
{
	RMPSession *session;

	if((session = _rmp_session_manager->Find(addr)) != 0)
	{
		_LOG "Error: session (" << session->GetIP() << ":" << session->GetPort() << ") is already connected!" EL;
		return session;
	}
	
	session = _rmp_session_manager->Insert(addr);
	session->OnDisconnectTask.Attach<RMPConnection>(this, &RMPConnection::DisconnectSession);
	session->OnPingTask.Attach<RMPConnection>(this, &RMPConnection::PingSession);
	session->OnDeliveryTask.Attach<RMPConnection>(this, &RMPConnection::DeliverySession);
	OnConnect(session);

	return session;
}

RMPSession *RMPConnection::Connect(const char *ip, uint16_t port)
{
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);

	return Connect((struct sockaddr *)&addr);
}

void RMPConnection::Disconnect(RMPSession *session)
{
	uint8_t id [4] = { 0, 0, 0, 0 };
	RMPMessageType type = RMP_MT_DISCONNECT;

	if(!SendTo(id, 4, (uint8_t *)&type, 1, (struct sockaddr *)&session->_sock_addr, sizeof(struct sockaddr_in)))
	{
		LOG "Error: cannot send disconnect message!" EL;
	}

	OnDisconnect(session);
	_rmp_session_manager->Delete(session);
}

void RMPConnection::SystemRecv(struct sockaddr *addr, uint32_t &id, RMPMessageType &type, uint8_t *data, uint16_t &size)
{
	RMPSession *session;

	if((session = _rmp_session_manager->Find(addr)) == 0)
	{
		session = _rmp_session_manager->Insert(addr);
		session->OnDisconnectTask.Attach<RMPConnection>(this, &RMPConnection::DisconnectSession);
		session->OnPingTask.Attach<RMPConnection>(this, &RMPConnection::PingSession);
		session->OnDeliveryTask.Attach<RMPConnection>(this, &RMPConnection::DeliverySession);
		OnConnect(session);
	}
	else
	{
		session->_last_recv_time.Update();
	}

	OnRecv(session, id, type, data, size);
}

void RMPConnection::DisconnectSession(RMPSession *session)
{
	_sessions.push(session);
	_core->ScheduleAdd(_task_disconnect);
}

void RMPConnection::PingSession(RMPSession *session)
{
	OnPingTask(session);
}

void RMPConnection::DeliverySession(RMPSession *session)
{
	OnDeliveryTask(session);
}

void RMPConnection::TaskDisconnect()
{
	while(_sessions.size() > 0)
	{
		OnDisconnect(_sessions.front());
		_rmp_session_manager->Delete(_sessions.front());
		_sessions.pop();
	}
}