#ifndef RMPSESSION_H
#define RMPSESSION_H

#include <list>
#include "RMPMessage.h"

namespace RMP
{
	#define RMP_RTT_START_VAL 1000
	#define RMP_RTT_MIN_VALUE 1000

	enum RMPGlewState 
	{ 
		RMP_GS_IDLE = 0, 
		RMP_GS_PART = 1 
	};

	struct OrderIdLess : public std::binary_function<uint32_t ,uint32_t, bool>
	{
		bool operator()(const uint32_t &id1, const uint32_t &id2) const
		{
			bool ret = id1 < id2;

			uint32_t diff;
			const uint32_t quarter_max = MAXUINT32 / 4;

			if (ret)
				diff = id2 - id1;
			else
				diff = id1 - id2;

			if (diff > quarter_max)
				return !ret;
			else
				return ret;
		}
	};

	class RMPSession
	{
	private:
		CoreObject							*_core;
		
		struct sockaddr_in					 _sock_addr;

		/* MESSAGING */
		std::map<uint32_t, RMPMessage *, OrderIdLess>	 _recv_messages;
		std::map<uint32_t, RMPMessage *, OrderIdLess>	 _delivery_messages;

		uint32_t							 _last_recv_id;
		uint32_t							 _last_sent_id;

		Time								 _last_recv_time;

		/* SEND QUEUE */
		std::queue<RMPMessage *>			 _send_queue;
		uint32_t							 _send_queue_size;
		uint32_t							 _send_queue_speed;
		uint32_t							 _send_queue_free_space;
		Mutex								*_mutex_send;

		/* SCHEDULER */
		Scheduler							*_scheduler;

		/* TIME TASK CONNECTION */
		TimeTask							*_task_connection;
		void								 TaskConnection();
		Event1<RMPSession *>				 OnDisconnectTask;

		/* TIME TASK DELIVERY */
		TimeTask							*_task_delivery;
		void								 TaskDelivery();
		Event1<RMPSession *>				 OnDeliveryTask;

		/* TIME TASK PING */
		TimeTask							*_task_ping;
		void								 TaskPing();
		Event1<RMPSession *>				 OnPingTask;

		/* RTT */
		std::list<uint64_t>					 _rtts;
		uint64_t							 _last_rtt;
		uint64_t							 _avg_rtt;

		/* SPEED */
		uint64_t							 _speed_download;
		uint64_t							 _speed_upload;
		uint64_t							 _speed_sys_download;
		uint64_t							 _speed_sys_upload;
		uint64_t							 _speed_cur_download;
		uint64_t							 _speed_cur_upload;
		uint64_t							 _speed_cur_sys_download;
		uint64_t							 _speed_cur_sys_upload;

		/* GLEW */
		RMPGlewState						 _glew_state;
		uint16_t							 _glew_buffer_seek;
		uint16_t							 _glew_buffer_size;
		uint8_t								*_glew_buffer;

		/* CONGESTION */
		std::map<uint32_t, uint32_t>		 _congested_ids;
		bool								 _congested;
	public:
		RMPSession(struct sockaddr *, CoreObject *);
		~RMPSession();

		/* Get ip-address, port and ip version */
		char		*GetIP()	{ return inet_ntoa(this->_sock_addr.sin_addr); }
		uint16_t	 GetPort()	{ return ntohs(this->_sock_addr.sin_port); }

		/* Get all recv and delivery messages */
		void GetAllRecvMessages(std::map<uint32_t, RMPMessage *, OrderIdLess> *);
		void GetAllDeliveryMessages(std::map<uint32_t, RMPMessage *, OrderIdLess> *);

		/* Get rtt values */
		uint64_t	GetAverageRtt()		{ return _avg_rtt;  }
		uint64_t	GetLastRtt()		{ return _last_rtt; }

		/* Get speed values */
		uint64_t	GetSpeedSysDownload()		{ return _speed_cur_sys_download;	}
		uint64_t	GetSpeedSysUpload()			{ return _speed_cur_sys_upload;		}
		uint64_t	GetSpeedDownload()			{ return _speed_cur_download;		}
		uint64_t	GetSpeedUpload()			{ return _speed_cur_upload;			}

		/* Get congestion state */
		bool		Congested()			{ return _congested; }

		/* Friend classes */
		friend class RMPSessionManager;
		friend class RMPConnection;
		friend class RMPMessaging;
		friend class RMPDelivery;
		friend class RMPRtt;
		friend class RMPCongestion;
		friend class RMPPing;
		friend class RMPOrder;
		friend class RMPGlew;
		friend class RMPSpeed;
	};
}

#endif //RMPSESSION_H