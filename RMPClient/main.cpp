/* CLIENT */

#include "../src/RMPNode.h"
#pragma comment (lib, "../Release/RMP")
#pragma comment (lib, "CoreObject")
using namespace RMP;

class Client
{
	CoreObject		*_core;
	Timer			*_timer;

	RMPNode			*_node;

	uint8_t				*_data;
	uint16_t			 _size;

	uint32_t			 _i;

	RMPSession	*_session;
public:

	Client(CoreObject *core) : _core(core)
	{
		_i = 0;
		_size = 65535;
		_data = fast_alloc(uint8_t, _size);
		memset(_data, 8, _size);

		fast_new1(RMPNode, _node, _core);
		
		_node->OnConnect.Attach<Client>(this, &Client::OnConnectCallback);
		_node->OnDisconnect.Attach<Client>(this, &Client::OnDisconnectCallback);
		_node->OnCongestionStart.Attach<Client>(this, &Client::OnCongestionStartCallback);
		_node->OnCongestionEnd.Attach<Client>(this, &Client::OnCongestionEndCallback);

		_node->Bind(2468);

		_session = _node->Connect("127.0.0.1", 1357);
	}
	~Client()
	{
		fast_delete(RMPNode, _node);
	}

	void OnConnectCallback(RMPSession *session)
	{
		//_session = session;
		std::cout << "Connected new session: " << session->GetIP() << ":" << session->GetPort() << "." << std::endl;
	}

	void OnDisconnectCallback(RMPSession *session)
	{
		_session = 0;
		std::cout << "Disconnected session: " << session->GetIP() << ":" << session->GetPort() << "." << std::endl;
	}

	void OnCongestionStartCallback(RMPSession *session)
	{
		std::cout << "Congestion started in session: " << session->GetIP() << ":" << session->GetPort() << std::endl;
	}

	void OnCongestionEndCallback(RMPSession *session)
	{
		std::cout << "Congestion ended in session: " << session->GetIP() << ":" << session->GetPort() << std::endl;
	}

	void Send()
	{
		const uint32_t operations = 1000;
		uint16_t size = 65535;

		uint32_t total_size = 0;

		uint8_t *data = fast_alloc(uint8_t, size);
		memset(data, 8, size);

		for(uint32_t i = 0; i < operations; i++)
		{
			if(!_node->Send(_session, data, size))
			{
				std::cout << "Error: cannot send msg!" << std::endl;
			}
			total_size += size;
		}

		std::cout << "SENT " << total_size << " BYTE(-S)" << std::endl;

		fast_free(data);
	}
};

int main()
{
	CoreObject *_core = new CoreObject();
	
	Client *client = new Client(_core);

	std::cout << "Press enter!" << std::endl;
	std::cin.get();

	client->Send();
	Time t1;

	std::cin.get();

	Time t2;
	std::cout << "Time = " << t2 - t1 << std::endl;

	std::cin.get();
	delete client;

	std::cin.get();

	delete _core;
	return 0;
}